package com.example.luiz.handon1;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.Random;

public class MainActivity extends AppCompatActivity {
    private int bt1Clicked,bt2Clicked = 0;
    private TextView tv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tv = findViewById(R.id.textView);

    }

    public void randomColorPicker(View view) {

        Button b = (Button) view;
        Random rnd = new Random();
        int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
        b.setBackgroundColor(color);

        switch (view.getId()){
            case R.id.bt1:
                tv.setText(String.format("Pressed %d times",++bt1Clicked));
                break;

            case R.id.bt2:
                tv.setText(String.format("Pressed %d times",++bt2Clicked));
                break;
        }
    }
}
